app.controller("galeriaCtrl",function(servicios,$scope,blockUI){
    $scope.imagenes=[];
    
    var myBlockUI = blockUI.instances.get('myBlockUI');
    myBlockUI.start();
    $scope.imgPopUp
    $scope.openPop=false
    $scope.initView=function(){
        $scope.height=$(window).height()
        servicios.get('galeria/','',function(res){
            
            myBlockUI.stop();
            if(res.error){
                alert('Error consumiendo el servicio')
                
            }else{
                $scope.imagenes=res.response;
                console.log($scope.listas);
            }
        })           
    }
    $scope.openPopUp=function(imagen){
        $scope.imgPopUp=imagen
        $scope.openPop=true
    }
    $scope.closePopUp=function(){
        $scope.openPop=false
    }
    $scope.isLast=function(index){
        if(index==$scope.imagenes.length-1){
           $('.grid').masonry({
          // options...
          itemSelector: '.gridItem',
          columnWidth: '.img-responsive',
        percentPosition: true
        });
        setTimeout(function(){
            $('.grid').masonry({
          // options...
                itemSelector: '.gridItem',
                columnWidth: '.gridItem',
                percentPosition: true
            });
        },100)
        }
    }
})