app.controller("eventsCtrl",function(servicios,blockUI,$scope){
    var myBlockUI = blockUI.instances.get('myBlockUI');
    myBlockUI.start();
    var inputValue=""
    $scope.fechas=[];
    $scope.fechasDatePicker=[];
    $scope.horarios=[];
    var monthArray=['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    $scope.initView=function(){
        servicios.get('calendario/','',function(res){
            myBlockUI.stop();
            if(res.error){
                alert('Error consumiendo el servicio')
            }else{
                
                console.log(res);
                /*[
                    {
                      date: '2018/09/3',
                      value: ' '
                    },
                    {
                      date: '2018/09/4',
                      value: ' '
                    },
                    {
                      date: '2018/09/5',
                      value: ' '
                    }
                  ]*/
                $scope.fechas=res.response;
                for (var i = 0; i < $scope.fechas.length; i++) {
                   var arrFecha= $scope.fechas[i].fecha.split("/")
                    var fecha={
                        date:arrFecha[2]+"/"+arrFecha[0]+"/"+arrFecha[1],
                        value: ' '
                    }
                    
                     $scope.fechasDatePicker.push(fecha)
                    
                }
                $scope.newCalendar();
                
            }
        }) 
    }
   
    $scope.clearPicker=function(){
         $('#datepicker').val("").datepicker("update");
         
    }
    $scope.initBind=function(){
        
        setInterval(function(){
        	console.log($( "#my_hidden_input" ).val())
        	if($( "#my_hidden_input" ).val()!=inputValue){
        	    
        		inputValue=$( "#my_hidden_input" ).val()
        		$scope.UpdatePick()
        	}
        },100)
    }
    var flag=true
    $scope.headerPicker=function(){
        
        var headerOriginalText=$(".calendar-display").text()
        setInterval(function(){
            var headerText=$(".calendar-display").text()
            if(headerText!=headerOriginalText||flag){
                var arrHeaderText=headerText.split("/");
                //$( "<p>Test</p>" ).appendTo( ".inner" );
                if(flag){
                    $("<div class='myDate'>"+monthArray[parseInt($(".calendar-display .m").text())-1]+" "+arrHeaderText[0]+"<div>").appendTo(".calendar-hd")
                }else{
                    $(".myDate").text(monthArray[parseInt($(".calendar-display .m").text())-1]+" "+arrHeaderText[0])
                }
                
                headerOriginalText=$(".calendar-display").text()
            }
            flag=false
        },200)
        
    }
    $scope.newCalendar=function(){
        $('.calender').calendar({
          data: $scope.fechasDatePicker,
          monthArray:monthArray,
          weekArray: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
          onSelected: function(view, date, value) {
              var dateWidthFormat=moment(date).format('MM/DD/YYYY')
              
              
              
              for (var i = 0; i < $scope.fechas.length; i++) {
                  var fecha=$scope.fechas[i].fecha
                  if(fecha==dateWidthFormat){
                      $scope.$apply(function() {
                        $scope.horarios=$scope.fechas[i].horarios
                    });
                      //angular block ui
                      
                  }
              }
          }
        });
        $scope.headerPicker()
        
    }
     $scope.initView();
     
    
})