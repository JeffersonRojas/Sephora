app.config(['$routeProvider','$locationProvider', function($routeProvider,$locationProvider) {
    $routeProvider
        .when('/events', {
            templateUrl: 'views/events.html',
            controller: 'eventsCtrl'
        })
        .when('/redes', {
            templateUrl: 'views/redes.html',
            controller:'redesCtrl'
        })
        .when('/horarios', {
            templateUrl: 'views/horarios.html',
            controller:'horariosCtrl'
        })        
        .when('/marcas', {
            templateUrl: 'views/marcas.html',
            controller:'marcasCtrl'
        })
        .when('/personas', {
            templateUrl: 'views/personas.html',
            controller:'personasCtrl'
        })
        .when('/galeria', {
            templateUrl: 'views/galeria.html',
            controller:'galeriaCtrl'
        })
        .when('/beauty', {
            templateUrl: 'views/beauty.html',
            controller:'beautyCtrl'
        })
        .otherwise({ redirectTo: '/events' });
    $locationProvider.hashPrefix("");
}])