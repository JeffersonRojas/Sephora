app.factory('servicios', function($http){
    var baseUrl="http://sephora-zironet.c9users.io/api/db/";
    return {
        "post":fcnPost,
        "get":fcnGet,
        "put":fcnPut,
        "delete":fcnDelete
    }
    
    function fcnGet(url,params,callback){
        $http({
          method: 'GET',
          url: baseUrl+url
        })
        .then(function successCallback(data) {
            callback(data.data)
        }, function errorCallback(response) {
            console.log('Error get '+url);
        });
    }
   
    function fcnPost(url,postData,callback){
        var data={};
        data.data=postData;
        $http({
          method: 'POST',
          url: baseUrl+url+'.json',
          data:data
        })
        .then(function successCallback(data) {
            callback(data.data)
        }, function errorCallback(response) {
            console.log('Error post '+url)
        });
    }
    function fcnPut(url,postData,callback){
        var data={};
        data.data=postData;
        $http({
          method: 'PUT',
          url: baseUrl+url+'.json',
          data:data
        })
        .then(function successCallback(data) {
            callback(data.data)
        }, function errorCallback(response) {
            console.log('Error put '+url)
        });
    }
    function fcnDelete(url,params,callback){
        $http({
          method: 'DELETE',
          url: baseUrl+url+'.json?id='+params,
        })
        .then(function successCallback(data) {
            callback(data.data)
        }, function errorCallback(response) {
            console.log('Error delete '+url)
        });
    }
});