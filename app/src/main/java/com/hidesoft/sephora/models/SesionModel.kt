package com.hidesoft.sephora.models

data class SesionModel(

        var nombre: String = "",

        var correo: String = "",

        var token: String = ""

)