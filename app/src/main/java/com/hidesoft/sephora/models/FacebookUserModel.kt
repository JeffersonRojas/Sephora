package com.hidesoft.sephora.models

data class FacebookUserModel(

        var email: String = "",

        var name: String = "",

        var id: String = ""

)