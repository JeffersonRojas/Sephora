package com.hidesoft.sephora.models

data class RegistroModel(

        var correo: String = "",

        var clave: String = "",

        var nombre: String = ""

)