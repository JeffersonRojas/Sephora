package com.hidesoft.sephora.application

import android.support.multidex.MultiDexApplication

@Suppress("unused")
class Sephora : MultiDexApplication()
