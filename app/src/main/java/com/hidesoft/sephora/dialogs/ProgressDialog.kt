package com.hidesoft.sephora.dialogs

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AlertDialog
import com.hidesoft.sephora.databinding.ProgressDialogBinding

class ProgressDialog : BaseDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val activity = this.activity ?: return super.onCreateDialog(savedInstanceState)
        val builder = AlertDialog.Builder(activity)
        val binding = ProgressDialogBinding.inflate(activity.layoutInflater)
        builder.setView(binding.root)
        val dialog = builder.create()
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
        return dialog
    }

}

fun FragmentActivity?.showProgressDialog() {
    this?.let {
        val dialog = ProgressDialog()
        dialog.show(supportFragmentManager, ProgressDialog::class.java.name)
    }
}

fun FragmentActivity?.dismissProgressDialog() {
    val progressDialog = this?.supportFragmentManager?.findFragmentByTag(ProgressDialog::class.java.name)
    if (progressDialog is ProgressDialog) {
        progressDialog.dismissAllowingStateLoss()
    }
}

fun Fragment.showProgressDialog() = activity.showProgressDialog()

fun Fragment.dismissProgressDialog() = activity.dismissProgressDialog()