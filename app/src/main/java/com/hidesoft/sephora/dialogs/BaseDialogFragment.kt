package com.hidesoft.sephora.dialogs

import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import com.crashlytics.android.Crashlytics

abstract class BaseDialogFragment : DialogFragment() {

    override fun show(manager: FragmentManager?, tag: String?) {
        val fragment = manager?.findFragmentByTag(tag)
        val ft = manager?.beginTransaction()
        if (fragment != null) {
            ft?.remove(fragment)
            ft?.commitAllowingStateLoss()
        }
        try {
            super.show(manager, tag)
        } catch (exception: Exception) {
            Crashlytics.log("Crash show dialog fragment isAdded : $isAdded previusFragment != null : ${fragment != null} tag: $tag")
            Crashlytics.logException(exception)
        }
    }

    override fun dismissAllowingStateLoss() {
        try {
            super.dismissAllowingStateLoss()
        } catch (exception: Exception) {
            Crashlytics.log("Crash dismissAllowingStateLoss dialog fragment isAdded : $isAdded tag: $tag")
            Crashlytics.logException(exception)
        }
    }

    override fun onStart() {
        super.onStart()
        isCancelable = false
    }

}