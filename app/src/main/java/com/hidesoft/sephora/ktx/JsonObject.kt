package com.hidesoft.sephora.ktx

import com.google.gson.JsonObject

val JsonObject.haveError: Boolean
    get() {
        val jsonElement = this.get("error")
        return if (jsonElement.isJsonPrimitive) {
            jsonElement.asString != "0"
        } else {
            true
        }
    }

val JsonObject.error: String
    get() {
        val jsonElement = this.get("response")
        return if (jsonElement.isJsonArray || jsonElement.isJsonObject) {
            jsonElement.toString()
        } else {
            jsonElement.asString
        }
    }

val JsonObject.response: String
    get() {
        val jsonElement = this.get("response")
        return if (jsonElement.isJsonArray || jsonElement.isJsonObject) {
            jsonElement.toString()
        } else if (jsonElement.isJsonPrimitive) {
            this.toString()
        } else {
            jsonElement.asString
        }
    }