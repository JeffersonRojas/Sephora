package com.hidesoft.sephora.ktx

import android.support.v4.app.Fragment
import android.widget.Toast

fun Fragment.showError(error: String?) {
    Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
}