package com.hidesoft.sephora.ktx

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


fun <T> Single<T>.apiRxConfig() = this.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())!!