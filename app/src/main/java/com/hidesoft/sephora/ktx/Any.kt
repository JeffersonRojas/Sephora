package com.hidesoft.sephora.ktx

import android.content.Context
import com.google.gson.Gson

fun Any.save(context: Context?) {
    if (context == null) return
    val sharedPreferences = context.getSharedPreferences("preferences", Context.MODE_PRIVATE)
    val editor = sharedPreferences.edit()
    editor.putString(this::class.java.name, this.toJson())
    editor.apply()
}

fun Any.toJson(): String = Gson().toJson(this)