package com.hidesoft.sephora.ktx

import android.content.Context

fun <T> Context.load(classOfT: Class<T>): T {
    val sharedPreferences = this.getSharedPreferences("preferences", Context.MODE_PRIVATE)
    val json = sharedPreferences.getString(classOfT.name, "") ?: ""
    return if (json.isEmpty()) "{}" to classOfT else json to classOfT
}