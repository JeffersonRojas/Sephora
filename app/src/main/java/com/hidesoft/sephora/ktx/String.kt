package com.hidesoft.sephora.ktx

import android.util.Patterns
import com.google.gson.Gson

fun String.isEmail() = Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun String.isValidPassword() = this.matches(Regex("^.{6,}\$"))

infix fun <T> String.to(classOfT: Class<T>): T = Gson().fromJson(this, classOfT)