package com.hidesoft.sephora.repository

import android.os.Build
import com.crashlytics.android.Crashlytics
import com.google.gson.JsonParser
import com.hidesoft.sephora.BuildConfig
import com.hidesoft.sephora.ktx.error
import com.hidesoft.sephora.ktx.haveError
import com.hidesoft.sephora.ktx.response
import okhttp3.*
import okhttp3.Interceptor
import okio.Buffer

class Interceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val nombreServicio = chain.request().url().pathSegments().last().split(".").first()
        Crashlytics.log("servicio : $nombreServicio")
        Crashlytics.log("metodo : ${chain.request().method()}")

        val response = when (chain.request().method()) {
            "POST" -> {
                val buffer = Buffer()
                chain.request().body()?.writeTo(buffer)
                val bufferUtf8 = buffer.readUtf8()
                val mediaType = MediaType.parse("application/json; charset=UTF-8")
                val jsonObject = JsonParser().parse(bufferUtf8)
                Crashlytics.log("body : $jsonObject")
                val requestBody = RequestBody.create(mediaType, jsonObject.toString())
                val request = chain.request().newBuilder()
                        .headers(chain.request().headers())
                        .header("Content-Type", requestBody.contentType().toString())
                        .header("Content-Length", requestBody.contentLength().toString())
                        .header("X-MC-SO", "android")
                        .header("X-MC-SO-V", Build.VERSION.RELEASE)
                        .header("X-MC-SO-API", Build.VERSION.SDK_INT.toString())
                        .header("X-MC-SO-PHONE-F", Build.MANUFACTURER)
                        .header("X-MC-SO-PHONE-M", Build.MODEL)
                        .header("X-MC-APP-V", BuildConfig.VERSION_NAME)
                        .method(chain.request().method(), requestBody).build()
                chain.proceed(request)
            }
            else -> chain.proceed(chain.request().newBuilder()
                    .headers(chain.request().headers())
                    .header("X-MC-SO", "android")
                    .header("X-MC-SO-V", Build.VERSION.RELEASE)
                    .header("X-MC-SO-API", Build.VERSION.SDK_INT.toString())
                    .header("X-MC-SO-PHONE-F", Build.MANUFACTURER)
                    .header("X-MC-SO-PHONE-M", Build.MODEL)
                    .header("X-MC-APP-V", BuildConfig.VERSION_NAME).build())
        }
        val jsonResponse = response.body()?.string() ?: "{}"
        Crashlytics.log("response : $jsonResponse")

        val json = JsonParser().parse(jsonResponse).asJsonObject
        if (json.haveError) {
            throw Exception(json.error)
        } else {
            return response.newBuilder().body(ResponseBody.create(response.body()?.contentType(), json.response)).build()
        }
    }

}