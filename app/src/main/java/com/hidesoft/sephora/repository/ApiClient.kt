package com.hidesoft.sephora.repository

import com.hidesoft.sephora.BuildConfig
import com.hidesoft.sephora.ktx.apiRxConfig
import com.hidesoft.sephora.models.LoginModel
import com.hidesoft.sephora.models.RegistroModel
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import okhttp3.OkHttpClient
import okhttp3.internal.platform.Platform
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {

    private val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(Interceptor())
            .addInterceptor(LoggingInterceptor.Builder()
                    .loggable(BuildConfig.DEBUG)
                    .setLevel(Level.BASIC)
                    .log(Platform.INFO)
                    .request("Api Request")
                    .response("Api Response")
                    .build())
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES)

    private val api: Api = Retrofit.Builder()
            .client(okHttpClient.build())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BuildConfig.BASE_URL)
            .build()
            .create(Api::class.java)

    fun registro(registroModel: RegistroModel) = api.registro(registroModel).apiRxConfig()

    fun login(loginModel: LoginModel) = api.login(loginModel).apiRxConfig()

}