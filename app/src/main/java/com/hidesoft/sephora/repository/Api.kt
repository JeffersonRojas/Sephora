package com.hidesoft.sephora.repository

import com.hidesoft.sephora.models.LoginModel
import com.hidesoft.sephora.models.RegistroModel
import com.hidesoft.sephora.models.SesionModel
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface Api {

    @POST("registro/")
    fun registro(@Body body: RegistroModel): Single<SesionModel>

    @POST("login/")
    fun login(@Body body: LoginModel): Single<SesionModel>

}