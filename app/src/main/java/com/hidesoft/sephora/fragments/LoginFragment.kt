package com.hidesoft.sephora.fragments

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.fragment.findNavController
import com.crashlytics.android.Crashlytics
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.hidesoft.sephora.R
import com.hidesoft.sephora.databinding.LoginFragmentBinding
import com.hidesoft.sephora.dialogs.dismissProgressDialog
import com.hidesoft.sephora.dialogs.showProgressDialog
import com.hidesoft.sephora.ktx.*
import com.hidesoft.sephora.models.FacebookUserModel
import com.hidesoft.sephora.models.LoginModel
import com.hidesoft.sephora.models.RegistroModel
import com.hidesoft.sephora.repository.ApiClient
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.login_fragment.*
import java.util.*

class LoginFragment : Fragment(), FacebookCallback<LoginResult> {

    private val callbackManager = CallbackManager.Factory.create()

    lateinit var binding: LoginFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.login_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.view = this
        binding.loginModel = LoginModel()

        setImage(ivHeader, R.drawable.ic_header_login)
        setImage(ivFooter, R.drawable.ic_footer_login)

        LoginManager.getInstance().registerCallback(callbackManager, this)
    }

    private fun setImage(iv: ImageView, resId: Int) {
        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        Picasso.get().load(resId).resize(displayMetrics.widthPixels, 0).into(iv)
    }

    fun login() {
        val loginModel = binding.loginModel ?: LoginModel()
        if (!loginModel.correo.isEmail()) {
            showError(getString(R.string.error_email))
            return
        }
        if (!loginModel.clave.isValidPassword()) {
            showError(getString(R.string.error_password))
            return
        }
        showProgressDialog()
        ApiClient.login(loginModel).subscribe { sesion, error ->
            dismissProgressDialog()
            if (error != null) {
                showError(error.message)
            } else {
                sesion.save(context)
                findNavController().navigate(R.id.action_login_to_home)
            }
        }
    }

    fun loginFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"))
    }

    override fun onSuccess(loginResult: LoginResult) {
        if (loginResult.recentlyDeniedPermissions.isNotEmpty()) {
            showError(getString(R.string.error_email_facebook))
            LoginManager.getInstance().logOut()
            return
        }

        val request = GraphRequest.newMeRequest(loginResult.accessToken) { _, response ->
            val userFacebook = response.rawResponse to FacebookUserModel::class.java
            val registroModel = RegistroModel(
                    nombre = userFacebook.name,
                    correo = userFacebook.email,
                    clave = userFacebook.id
            )
            showProgressDialog()
            ApiClient.registro(registroModel).subscribe { sesion, error ->
                dismissProgressDialog()
                if (error != null) {
                    showError(error.message)
                    LoginManager.getInstance().logOut()
                } else {
                    sesion.save(context)
                    findNavController().navigate(R.id.action_login_to_home)
                }
            }
        }

        val params = Bundle()
        params.putString("fields", "email, name")
        request.parameters = params
        request.executeAsync()
    }

    override fun onCancel() {

    }

    override fun onError(exception: FacebookException) {
        Crashlytics.logException(exception)
    }

    fun registro() {
        findNavController().navigate(R.id.action_login_to_registro)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

}