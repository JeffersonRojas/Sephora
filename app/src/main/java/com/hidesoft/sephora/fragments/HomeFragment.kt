package com.hidesoft.sephora.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.hidesoft.sephora.R
import com.hidesoft.sephora.databinding.HomeFragmentBinding
import android.content.Intent
import android.net.Uri


class HomeFragment : Fragment() {

    val action = HomeFragmentDirections.actionHomeToWeb()

    lateinit var binding: HomeFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)
        binding.view = this
        return binding.root
    }

    fun openMaster() {
        action.setUrl(getString(R.string.url_horarios))
        action.setTitle(getString(R.string.master_classes))
        findNavController().navigate(action)
    }

    fun openBeauty() {
        action.setUrl(getString(R.string.url_beauty))
        action.setTitle(getString(R.string.beauty_pass))
        findNavController().navigate(action)
    }

    fun openCalendario() {
        action.setUrl(getString(R.string.url_eventos))
        action.setTitle(getString(R.string.calendario))
        findNavController().navigate(action)
    }

    fun openMarcas() {
        action.setUrl(getString(R.string.url_marcas))
        action.setTitle(getString(R.string.marcas_participantes))
        findNavController().navigate(action)
    }

    fun openMakeup() {
        action.setUrl(getString(R.string.url_personas))
        action.setTitle(getString(R.string.makeup_artist))
        findNavController().navigate(action)
    }

    fun openGaleria() {
        action.setUrl(getString(R.string.url_galeria))
        action.setTitle(getString(R.string.galeria))
        findNavController().navigate(action)
    }

    fun openWeb() {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.url_sephora)))
        startActivity(browserIntent)
    }

    fun openRedes() {
        action.setUrl(getString(R.string.url_redes))
        action.setTitle(getString(R.string.redes_sociales))
        findNavController().navigate(action)
    }

}