package com.hidesoft.sephora.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.fragment.findNavController
import com.hidesoft.sephora.R
import com.hidesoft.sephora.databinding.RegistroFragmentBinding
import com.hidesoft.sephora.dialogs.dismissProgressDialog
import com.hidesoft.sephora.dialogs.showProgressDialog
import com.hidesoft.sephora.ktx.isEmail
import com.hidesoft.sephora.ktx.isValidPassword
import com.hidesoft.sephora.ktx.save
import com.hidesoft.sephora.ktx.showError
import com.hidesoft.sephora.models.RegistroModel
import com.hidesoft.sephora.repository.ApiClient
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.registro_fragment.*

class RegistroFragment : Fragment() {

    lateinit var binding: RegistroFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.registro_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.view = this
        binding.registroModel = RegistroModel()

        setImage(ivHeader, R.drawable.ic_header_login)
        setImage(ivFooter, R.drawable.ic_footer_login)
    }

    private fun setImage(iv: ImageView, resId: Int) {
        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        Picasso.get().load(resId).resize(displayMetrics.widthPixels, 0).into(iv)
    }

    fun registro() {
        val registroModel = binding.registroModel ?: RegistroModel()
        if (registroModel.nombre.isEmpty()) {
            showError(getString(R.string.error_nombre))
            return
        }
        if (!registroModel.correo.isEmail()) {
            showError(getString(R.string.error_email))
            return
        }
        if (!registroModel.clave.isValidPassword()) {
            showError(getString(R.string.error_password))
            return
        }
        showProgressDialog()
        ApiClient.registro(registroModel).subscribe { sesion, error ->
            dismissProgressDialog()
            if (error != null) {
                showError(error.message)
            } else {
                sesion.save(context)
                findNavController().navigate(R.id.action_registro_to_home)
            }
        }
    }
}