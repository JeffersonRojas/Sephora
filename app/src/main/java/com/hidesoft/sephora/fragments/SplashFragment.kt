package com.hidesoft.sephora.fragments

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.util.DisplayMetrics
import android.widget.ImageView
import androidx.navigation.fragment.findNavController
import com.hidesoft.sephora.R
import com.hidesoft.sephora.ktx.load
import com.hidesoft.sephora.models.SesionModel
import com.squareup.picasso.Picasso

class SplashFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val goToLogin = context?.load(SesionModel::class.java)?.correo.isNullOrEmpty()
        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        Picasso.get().load(R.drawable.ic_footer_login).resize(displayMetrics.widthPixels, 0).into(ImageView(context))
        Picasso.get().load(R.drawable.ic_header_login).resize(displayMetrics.widthPixels, 0).into(ImageView(context))
        Handler().postDelayed({
            activity?.window?.setBackgroundDrawableResource(R.color.colorPrimary)
            if (goToLogin) {
                findNavController().navigate(R.id.action_splash_to_login)
            } else {
                findNavController().navigate(R.id.action_splash_to_home)
            }
        }, 1200)
    }

}