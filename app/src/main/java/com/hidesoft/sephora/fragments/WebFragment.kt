package com.hidesoft.sephora.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import androidx.navigation.fragment.findNavController
import com.hidesoft.sephora.BuildConfig
import com.hidesoft.sephora.R
import com.hidesoft.sephora.databinding.WebFragmentBinding
import kotlinx.android.synthetic.main.web_fragment.*

class WebFragment : Fragment() {

    lateinit var binding: WebFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.web_fragment, container, false)
        return binding.root
    }

    @SuppressLint("SetJavaScriptEnabled", "AddJavascriptInterface")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showProgress(true)
        webView?.webChromeClient = object : WebChromeClient() {}
        webView?.webViewClient = object : WebViewClient() {

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                showProgress(true)
            }

            @Suppress("OverridingDeprecatedMember")
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                if (url == null) return false
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                return true
            }

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    val url = request?.url
                    url?.let {
                        startActivity(Intent(Intent.ACTION_VIEW, it))
                        return true
                    }
                }
                return false
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                showProgress(false)
            }
        }
        webView?.settings?.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        webView?.settings?.pluginState = WebSettings.PluginState.ON
        webView?.settings?.allowFileAccess = true
        webView?.settings?.allowContentAccess = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            webView?.settings?.allowFileAccessFromFileURLs = true
            webView?.settings?.allowUniversalAccessFromFileURLs = true
        }
        webView?.settings?.javaScriptCanOpenWindowsAutomatically = true
        webView?.settings?.javaScriptEnabled = true
        webView?.settings?.domStorageEnabled = true
        webView?.loadUrl(WebFragmentArgs.fromBundle(arguments).url)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(BuildConfig.DEBUG)
        }
        btnBack?.setOnClickListener {
            findNavController().popBackStack()
        }
        tvTitle?.text = WebFragmentArgs.fromBundle(arguments).title
    }

    private fun showProgress(value: Boolean) {
        loading?.visibility = if (value) View.VISIBLE else View.GONE
    }

}