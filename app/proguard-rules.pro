-keepattributes Signature

-keepattributes *Annotation*

-keepattributes SourceFile,LineNumberTable

-keep public class * extends java.lang.Exception

-keep class com.crashlytics.** { *; }

-dontwarn com.crashlytics.**

-keep class com.hidesoft.sephora.models.** { *; }

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

-keep class okio.** { *; }

-dontwarn okio.**

-keep class okhttp3.** { *; }

-dontwarn okhttp3.**

-keep class retrofit2.** { *; }

-dontwarn retrofit2.**

-keep class com.squareup.** { *; }

-dontwarn com.squareup.**

-dontwarn sun.misc.**

-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
   long producerIndex;
   long consumerIndex;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}

-dontnote rx.internal.util.PlatformDependent

-keep class com.facebook.** { *; }

-dontwarn com.google.zxing.**

-dontwarn bolts.**

-dontwarn com.google.errorprone.annotations.*

-keepclassmembers class * implements android.os.Parcelable {
    static ** CREATOR;
}

-keepattributes InnerClasses
